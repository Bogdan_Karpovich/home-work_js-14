  const currentTheme = localStorage.getItem('theme');
  if (currentTheme) {
    document.body.classList.add(currentTheme);
  }

  const themeButton = document.querySelector('.background__btn');

  themeButton.addEventListener('click', function() {
    document.body.classList.toggle('dark-theme');

    let theme = 'dark-theme';
    if (!document.body.classList.contains('dark-theme')) {
      theme = 'light-theme';
    }
    localStorage.setItem('theme', theme);
  });

